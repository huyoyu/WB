//
//  HHYPresentationController.swift
//  DB
//
//  Created by haixuan on 16/9/7.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class HHYPresentationController: UIPresentationController {
    // MARK:- 对外提供属性
    var presentedFrame : CGRect = CGRectZero
    
    // MARK:- 懒加载属性
    private lazy var converView : UIView = UIView()
    
    // MARK:- containerView容器,系统回调属性
    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
        
        // 1.设置弹出View的尺寸
        presentedView()?.frame = presentedFrame
        
        // 2.添加蒙版
        setupConverView()
    }
}


// MARK:- 设置UI界面相关
extension HHYPresentationController {
    private func setupConverView() {
        // 1.添加蒙版
        containerView?.insertSubview(converView, atIndex: 0)
        
        // 2.设置蒙版属性
        converView.backgroundColor = UIColor(white: 0.8, alpha: 0.2)
        converView.frame = containerView!.bounds
        
        // 3.添加手势
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(HHYPresentationController.converViewClick))
        converView.addGestureRecognizer(tapGesture)
        
    }
}

// MARK:- 事件监听
extension HHYPresentationController {
    @objc private func converViewClick() {
        presentedViewController.dismissViewControllerAnimated(true, completion: nil)
    }
}



