//
//  PopoverAnimator.swift
//  DB
//
//  Created by haixuan on 16/9/7.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class PopoverAnimator: NSObject {
    
    // MARK:- 对外提供的属性
    var isPresented : Bool = false
    var presentedFrame : CGRect = CGRectZero
    var callBack : ((Presented : Bool) -> ())?
    
    override init() {
        
    }
    // MARK:- 自定义构造函数
    // 注意:如果自定义了一个构造函数,但是没有对默认的构造函数init()进行重写,那么自定义的构造函数会覆盖默认的init()构造函数
    init(callBack : (presentedBack : Bool) -> ()) {
        self.callBack = callBack
    }
}


// MARK:- zing定义转场代理方法
extension PopoverAnimator : UIViewControllerTransitioningDelegate {
    // 目的:改变弹出View的尺寸
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController, sourceViewController source: UIViewController) -> UIPresentationController? {
        let presentation = HHYPresentationController(presentedViewController: presented, presentingViewController: presenting)
        presentation.presentedFrame = presentedFrame
        return presentation
    }
    
    // 目的:自定义弹出View的动画
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresented = true
        
        callBack!(Presented : isPresented)
        
        return self
    }
    
    // 目的:自定义消失的View的动画
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresented = false
        callBack!(Presented : isPresented)
        return self
    }
}
// MARK:- 弹出和消失动画代理的方法
extension PopoverAnimator : UIViewControllerAnimatedTransitioning {
    // 动画执行的时间
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
    
    // 获取转场的上下文:可以通过转场上下文获取弹出的View和消失的View
    // UITransitionContextFromViewKey : 获取消失的View
    // UITransitionContextToViewKey : 获取弹出的View
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        isPresented ? animationForPresentedView(transitionContext) : animationForDismissedView(transitionContext)
    }
    
    // 自定义弹出动画
    private func animationForPresentedView(transitionContext: UIViewControllerContextTransitioning) {
        // 1.获取弹出的View
        let presentedView = transitionContext.viewForKey(UITransitionContextToViewKey)
        
        // 2.将弹出的View添加到containerView中
        transitionContext.containerView()?.addSubview(presentedView!)
        
        // 3.执行动画
        presentedView?.transform = CGAffineTransformMakeScale(1.0, 0.0)
        presentedView?.layer.anchorPoint = CGPointMake(0.5, 0)
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            presentedView?.transform = CGAffineTransformIdentity
        }) { (_) in
            // 告诉上下文完成了动画
            transitionContext.completeTransition(true)
        }
    }
    
    // 自定义消失动画
    private func animationForDismissedView(transitionContext: UIViewControllerContextTransitioning) {
        // 1.获取消失的View
        let dismissedView = transitionContext.viewForKey(UITransitionContextFromViewKey)
        
        // 2.执行动画
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            dismissedView?.transform = CGAffineTransformMakeScale(1.0, 0.000001)
        }) { (_) in
            dismissedView?.removeFromSuperview()
            // 告诉上下文完成了动画
            transitionContext.completeTransition(true)
        }
    }
}
