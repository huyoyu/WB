//
//  StatusViewModel.swift
//  DB
//
//  Created by haixuan on 16/9/12.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class StatusViewModel: NSObject {
    // MARK:- 定义属性
    var status : StatusModel?
    
    // MARK:- 对数据处理的属性
    var sourceText : String?            // 处理来源
    var createAtText : String?          // 处理时间
    
    // MARK:- 用户数据处理
    var verifiedImage : UIImage?        // 处理用户认证图标
    var vipImage : UIImage?             // 处理用户会员等级
    
    var profileURL : NSURL?             // 处理用户头像的地址
    
    // MARK:- 自定义构造函数
    init(status : StatusModel) {
        self.status = status
        
        // 1>处理来源
        // 1.1nil值校验 = ""
        if let source = status.source where source != "" {
            // 1.2.对来源的字符串进行处理
            // <a href="http://app.weibo.com/t/feed/5g0B8s" rel="nofollow">微博weibo.com</a>
            // 2.1获取起始位置和截取长度
            let startIndex = (source as NSString).rangeOfString(">").location + 1
            let length = (source as NSString).rangeOfString("</").location - startIndex
            
            // 2.2截取字符串
            sourceText = (source as NSString).substringWithRange(NSRange(location: startIndex, length: length))
        }
        
        // 2>处理时间
        if let createAt = status.created_at {
            createAtText = NSDate.createDateString(createAt)
        }
        
        // 3.处理认证
        let verfidsType = status.user?.verified_type ?? -1
        switch verfidsType {
        case 1:
            // 个人认证
            verifiedImage = UIImage(named: "avatar_vip")
        case 2,3,5:
            // 企业认证
            verifiedImage = UIImage(named: "avatar_enterprise_vip")
        case 220:
            // 微博达人
            verifiedImage = UIImage(named: "avatar_grassroot")
        default:
            verifiedImage = nil
        }
        
        // 4.处理会员图标
        let mbrank = status.user?.mbrank ?? 0
        if mbrank > 0 && mbrank <= 6 {
            vipImage = UIImage(named: "common_icon_membership_level\(mbrank)")
        }
        
        // 5.用户头像的处理
        let profileUrlString = status.user?.profile_image_url ?? ""
        profileURL = NSURL(string: profileUrlString)
        
    }
}
