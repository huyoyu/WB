//
//  UserModel.swift
//  DB
//
//  Created by haixuan on 16/9/12.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class UserModel: NSObject {
    // MARK:- 用户属性
    var profile_image_url : String?     // 用户头像
    var screen_name : String?           // 用户的昵称
    var verified_type :Int = -1        // 用户的认证类型
    var mbrank : Int = 0                // 用户的会员等级
    
    // MARK:- 自定义构造函数
    init(dict : [String : AnyObject]) {
        super.init()
        
        setValuesForKeysWithDictionary(dict)
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
    }
}
