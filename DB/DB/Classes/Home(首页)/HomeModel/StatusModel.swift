//
//  StatusModel.swift
//  DB
//
//  Created by haixuan on 16/9/12.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class StatusModel: NSObject {
    // Mon Sep 12 09:25:02 +0800 2016
    var created_at : String?            // 微博创建时间
    var source : String?                // 微博来源
    var text : String?                  // 微博的正文
    var mid : Int = 0                   // 微博ID
    
    var user : UserModel?    // 用户
    
    // MARK:- 自定义构造函数
    init(dict : [String : AnyObject]) {
        super.init()
        
        setValuesForKeysWithDictionary(dict)
        
        // 1.将用户字典转成用户模型对象
        if let userDict = dict["user"] as? [String : AnyObject] {
            user = UserModel(dict: userDict)
        }
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
        
    }
}
