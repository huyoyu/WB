//
//  HomeViewController.swift
//  DB
//
//  Created by haixuan on 16/9/6.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    // MARK:- 懒加载属性
    private var titleBtn : TitleButton = TitleButton()
    // 注意:在闭包中如果使用当前对象的属性或者调用方法,也需要加self
    // 两个地方需要使用self : 1>如果在一个函数中出现歧义  2>在闭包中使用当前对象的属性和方法也需要加self
    private lazy var popoverAnimator : PopoverAnimator = PopoverAnimator {[weak self] (presented) in
        self?.titleBtn.selected = presented
    }
    
    private lazy var viewModels : [StatusViewModel] = [StatusViewModel]()
    
    // MARK:- 系统回调函数
    override func viewDidLoad() {
        super.viewDidLoad()

        // 1.没有登陆时设置ideo内容
        visitorView.addRotationAnimate()
        if !isLogin {
            return
        }
        
        // 2.设置导航栏的内容
        setupNavigationBar()
        
        // 3.请求数据
        loadStatuses()
        
        tableView.rowHeight  = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
    }
}

// MARK:- 设置UI界面
extension HomeViewController {
    private func setupNavigationBar() {
        // 1.设置左侧的item
        navigationItem.leftBarButtonItem = UIBarButtonItem(imageName: "navigationbar_friendattention")
        
        // 2.设置右侧的item
        navigationItem.rightBarButtonItem = UIBarButtonItem(imageName : "navigationbar_pop")
        
        // 3.设置titleView
        titleBtn.setTitle("huayoyu", forState: .Normal)
        titleBtn.addTarget(self, action: #selector(HomeViewController.titleBtnClick(_:)), forControlEvents: .TouchUpInside)
        navigationItem.titleView = titleBtn
    }
}

// MARK:- 事件的监听函数
extension HomeViewController {
    @objc private func titleBtnClick(titleBtn : TitleButton) {
//        // 1.改变按钮的状态
//        titleBtn.selected = !titleBtn.selected
        // 2.创建弹出的控制器
        let popoverVc = PopoverViewController()
        
        // 3.设置控制器的modal的样式
        popoverVc.modalPresentationStyle = .Custom
        
        // 4.设置转场的代理
        popoverVc.transitioningDelegate = popoverAnimator
        popoverAnimator.presentedFrame = CGRectMake(100, 55, 180, 250)
        
        // 3.弹出控制器
        presentViewController(popoverVc, animated: true, completion: nil)
    }
}

// MARK:- 请求数据
extension HomeViewController {
    private func loadStatuses() {
        NetworkTools.shareInstace.loadStatuses { (result, error) in
            // 1.错误校验
            if error != nil {
                print(error)
                return
            }
            
            // 2.获取可选类型中的数据
            guard let resultArray = result else {
                return
            }
            
            // 3.遍历微博中对应的字典
            for statsDict in resultArray {
                let status = StatusModel(dict: statsDict)
                let viewModel = StatusViewModel(status: status)
                self.viewModels.append(viewModel)
            }
            
            // 4.刷新表格
            self.tableView.reloadData()
        }
    }
}

// MARL:- tableView的数据源方法
extension HomeViewController {
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // 1.创建cell
        let cell = tableView.dequeueReusableCellWithIdentifier("HomeCell") as! HomeViewCell
        
        // 2.给cell设置数据
        cell.viewModel = viewModels[indexPath.row]
        
        return cell
    }
}




