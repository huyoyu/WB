//
//  MessageViewController.swift
//  DB
//
//  Created by haixuan on 16/9/6.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class MessageViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        visitorView.setupuVisitorViewInfo("visitordiscover_image_message", title: "登陆后,别人评论你的微博,给你发消息,都会在这里收到通知")
    }
}
