//
//  UIBarButton-Extension.swift
//  DB
//
//  Created by haixuan on 16/9/7.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
//    convenience init(imageName : String) {
//       self.init()
//        
//        let btn = UIButton()
//        btn.setImage(UIImage(named: imageName), forState: .Normal)
//        btn.setImage(UIImage(named: imageName + "_highlighted"), forState: .Highlighted)
//        btn.sizeToFit()
//        
//        self.customView = btn
//    }
    
    convenience init(imageName : String) {
        let btn = UIButton()
        btn.setImage(UIImage(named: imageName), forState: .Normal)
        btn.setImage(UIImage(named: imageName + "_highlighted"), forState: .Highlighted)
        btn.sizeToFit()
        
        self.init(customView : btn)
        
       
    }
}