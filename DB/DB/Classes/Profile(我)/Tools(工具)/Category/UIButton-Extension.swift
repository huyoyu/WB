//
//  UIButton-Extension.swift
//  DB
//
//  Created by haixuan on 16/9/6.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

extension UIButton {
    // swift中类方法是一class开头的方法,类似于OC中+开头的方法
    class func creatButton(imageName : String, bgImageName : String) -> UIButton {
        // 1.创建btn
        let btn = UIButton()
        
        // 2.设置btn的属性
        btn.setBackgroundImage(UIImage(named: bgImageName), forState: .Normal)
        btn.setBackgroundImage(UIImage(named: bgImageName + "_highlighted"), forState: .Highlighted)
        btn.setImage(UIImage(named: imageName), forState: .Normal)
        btn.setImage(UIImage(named: bgImageName +  "_highlighted"), forState: .Highlighted)
        btn.sizeToFit()
        
        return btn
    }
    
    // convenience : 便利,使用convenience修饰的构造函数叫做便利构造函数,通常用于在对系统的类进行构造函数的扩充时使用
    /**
     便利构造函数的特点
        1.便利构造函数通常都是写在extension里面
        2.便利构造函数init前面需要加上convenience
        3.在便利构造函数中需要明确调用self.init()
     */
    convenience init (imageName : String, bgImageName : String) {
        self.init()
        setImage(UIImage(named: imageName), forState: .Normal)
        setImage(UIImage(named: bgImageName +  "_highlighted"), forState: .Highlighted)
        setBackgroundImage(UIImage(named: bgImageName), forState: .Normal)
        setBackgroundImage(UIImage(named: bgImageName + "_highlighted"), forState: .Highlighted)
        sizeToFit()
    }
}















