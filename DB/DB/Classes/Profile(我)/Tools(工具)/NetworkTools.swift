//
//  NetworkTools.swift
//  AF封装
//
//  Created by haixuan on 16/9/7.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import AFNetworking

// 定义枚举类型
enum RequestType : String {
    case GET = "GET"
    case POST = "POST"
}

class NetworkTools: AFHTTPSessionManager {
    // let是线程安全的
    static let shareInstace : NetworkTools = {
       let tools = NetworkTools()
        tools.responseSerializer.acceptableContentTypes?.insert("text/html")
        tools.responseSerializer.acceptableContentTypes?.insert("text/plain")
        return tools
    }()
}

// MARK:- 封装请求方法
extension NetworkTools {
    func request(methodType : RequestType, urlString : String, parameters : [String : AnyObject], finished : (result : AnyObject?, error : NSError?) -> ()) {
        
        // 1.定义成功的回调闭包
        let successCallBack = { (task : NSURLSessionDataTask, result : AnyObject?) in
            finished(result: result, error: nil)
        }
        
        // 2.定义失败的回调闭包
        let failureCallBack = { (task : NSURLSessionDataTask?, error : NSError) in
            finished(result: nil, error: error)
        }
        
        if methodType == .GET {
            GET(urlString, parameters: parameters, progress: nil, success: successCallBack, failure: failureCallBack)

        } else {
            POST(urlString, parameters: parameters, progress: nil, success: successCallBack, failure: failureCallBack)
        }
        
    }

}

// MARK:- 请求AccessTocken
extension NetworkTools {
    func loadAccessToken(code : String, finished : (result : [String : AnyObject]?, error : NSError?) -> ()) {
        // 1.获取请求的URLString
        let urlString = "https://api.weibo.com/oauth2/access_token"
        
        // 2.获取请求的参数
        let parameter = ["client_id" : app_key,
                         "client_secret" : app_seret,
                         "grant_type" : "authorization_code",
                         "code" : code,
                         "redirect_uri" : redirect_uri]
        
        // 3.发送网络请求
        request(.POST, urlString: urlString, parameters: parameter) { (result, error) in
            finished(result: result as? [String : AnyObject], error: error)
        }
    }
}

// MAKR:- 请求用户信息
extension NetworkTools {
    func loadUserInfo(access_token : String, uid : String, finished : (result : [String : AnyObject]?, error : NSError?) -> ()) {
        // 1.获取请求的url
        let urlString = "https://api.weibo.com/2/users/show.json"
        
        // 2.获取请求的参数
        let parameter = ["access_token" : access_token,
                         "uid" : uid]
        
        // 3.发送网络请求
        request(.GET, urlString: urlString, parameters: parameter) { (result, error) in
            finished(result: result as? [String : AnyObject], error: error)
        }
    }
}

// MARK:- 请求首页数据
extension NetworkTools {
    func loadStatuses(finished : (result : [[String : AnyObject]]?, error : NSError?) -> ()){
        // 1.获取请求的URLString
        let urlString = "https://api.weibo.com/2/statuses/home_timeline.json"
        // https://api.weibo.com/2/statuses/home_timeline.json?access_token=2.00HNzsSDldfjaB50fa35e8f7FnzKPE
        // 2.获取请求的参数
        let paramaters = ["access_token" : (UserAccountTool.shareInstace.account?.access_token)!]
        print(paramaters)
        // 3.发送网络请求
        request(.GET, urlString: urlString, parameters: paramaters) { (result, error) in
            // 获取字典的数据
            guard let resultDict = result as? [String : AnyObject] else {
                finished(result: nil, error: error)
                return
            }
            
            // 2.将数组数据回调给外界控制器
            finished(result: resultDict["statuses"] as? [[String : AnyObject]], error: error)
        }
    }
}


