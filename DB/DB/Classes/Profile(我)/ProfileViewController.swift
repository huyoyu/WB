//
//  ProfileViewController.swift
//  DB
//
//  Created by haixuan on 16/9/6.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        visitorView.setupuVisitorViewInfo("visitordiscover_image_profile", title: "登陆后,你的微博、相册、个人资料会显示在这里,展示给别人")
    }

}
