//
//  VisitorView.swift
//  DB
//
//  Created by haixuan on 16/9/7.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class VisitorView: UIView {

    // MARK:- 提供快速通过xib创建的类方法
    class func visitorView() -> VisitorView {
        return NSBundle.mainBundle().loadNibNamed("VisitorView", owner: nil, options: nil).first as! VisitorView
    }
    
    // MAKR:- 控件属性
    @IBOutlet weak var rotationView: UIImageView!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var tipLab: UILabel!
    @IBOutlet weak var registedBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    
    // MARK:- 自定义函数
    func setupuVisitorViewInfo(iconName : String, title : String) {
        iconView.image = UIImage(named: iconName)
        tipLab.text = title
        rotationView.hidden = true
    }
    
    func addRotationAnimate( ) {
        // 1.创建动画
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        
        // 2.设置动画的属性
        rotationAnimation.fromValue = 0
        rotationAnimation.toValue = M_PI * 2
        rotationAnimation.repeatCount = MAXFLOAT
        rotationAnimation.duration = 5
        
        rotationAnimation.removedOnCompletion = false
        
        // 3.将动画添加到layer中
        rotationView.layer.addAnimation(rotationAnimation, forKey: nil)
    }
}







