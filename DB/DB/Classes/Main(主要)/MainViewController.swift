//
//  MainViewController.swift
//  DB
//
//  Created by haixuan on 16/9/6.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    // MARK:- 懒加载属性
    private lazy var imageNames = ["tabbar_home","tabbar_message_center","","tabbar_discover","tabbar_profile"]
    
//    private lazy var composeBtn : UIButton =  UIButton.creatButton("tabbar_compose_icon_add", bgImageName: "tabbar_compose_button")
    private lazy var composeBtn : UIButton = UIButton(imageName: "tabbar_compose_icon_add", bgImageName: "tabbar_compose_button")
    
    // MARK:-系统回调属性
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupCompseBtn()
       

    }
    
    // MARK:- 调整tabbar需要在willAppear中调整
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setupTabbarItems()
    }
}

// MARK:- 设置UI界面
extension MainViewController {
    // 设置发布按钮
    private func setupCompseBtn() {
        // 1.将composeBtn添加到tabbar中
        tabBar.addSubview(composeBtn)
        
        // 2.设置属性
        
        
        // 3.设置位置
        composeBtn.center = CGPointMake(tabBar.center.x, tabBar.bounds.size.height * 0.5)
        
        // 4.监听发布按钮的点击
        // Selector两种写法: 1> Selector("composeBtnClick") 2> "composeBtnClick"
        composeBtn.addTarget(self, action: #selector(MainViewController.composeBtnClick), forControlEvents: .TouchUpInside)
    }
    
    // 调整tabbar中的item 
    func setupTabbarItems() {
        // 1.遍历所有的item
        for i in 0..<tabBar.items!.count {
            // 2.获取item
            let item = tabBar.items![i]
            
            // 3.如果下标值为2,则该item不可以和用户交互
            if i == 2 {
                item.enabled = false
                continue
            }
            
            // 4.设置其他item的选中时候的图片
            item.selectedImage = UIImage(named: imageNames[i] + "_highlighted")
        }
    }
    
    //func setupNav() {
        /*
         // 1.通过代码
         //        addChildViewController("HomeViewController", title: "首页", imageName: "tabbar_home")
         //        addChildViewController("MessageViewController", title: "消息", imageName: "tabbar_message_center")
         //        addChildViewController("DiscoverViewController", title: "发现", imageName: "tabbar_discover")
         //        addChildViewController("ProfileViewController", title: "我", imageName: "tabbar_profile")
         
         // 2.通过json解析
         // 1.获取json文件路径
         guard let jsonPath = NSBundle.mainBundle().pathForResource("MainVCSettings.json", ofType: nil) else {
         print("没有获取到对应的文件路径")
         return
         }
         
         // 2.读取json文件中的内容
         guard let jsonData = NSData(contentsOfFile: jsonPath) else {
         print("没有获取到json文件中数据")
         return
         }
         
         // 3.将data转成数组
         // 如果在调用系统魔偶一个方法时,该方法最后有一个throws.说明该方法会抛出异常,那么需要对该异常记性处理
         /*
         在Swift中提供三种处理异常的方式
         方式一:try方式   程序员手动捕捉异常do try  catch
         方式二:try?方式  系统帮助我们处理异常,如果该方法发现了异常,则该方法返回nil,如果没有异常,则返回对应的对象
         方式三:try!方式   直接告诉系统,该方法没有异常.注意.如果该方法出现了异常,直接奔溃
         */
         guard let anyObject = try? NSJSONSerialization.JSONObjectWithData(jsonData, options: .MutableContainers) else {
         return
         }
         
         guard let dictArray = anyObject as? [[String : AnyObject]] else {
         return
         }
         
         // 4.遍历字典,获取对应的信息
         for dict in dictArray {
         // 4.1 获取控制器的对应的字符串
         guard let vcName = dict["vcName"] as? String else {
         continue
         }
         // 4.2 获取控制器显示的title
         guard let title = dict["title"] as? String else {
         continue
         }
         
         // 4.3 获取控制器显示的图标名称
         guard let imageName = dict["imageName"] as? String else {
         continue
         }
         
         // 4.4 添加子控制器
         addChildViewController(vcName, title: title, imageName: imageName)
         }
         }
         
         // swift支持方法的重载
         // 方法的重载:方法名称相同,但是参数不同.  -- > 1.参数的类型不同  2.参数的个数不同
         
         // private在当前文件中可以访问,但是其他文件不能访问
         private func addChildViewController(childVcName: String, title : String, imageName : String) {
         // 0.获取命名空间
         guard let nameSpace = NSBundle.mainBundle().infoDictionary!["CFBundleExecutable"] as? String else {
         print("没有获取到命名空间")
         return
         }
         
         // 1.根据字符串获取对应的Class
         guard let childVcClass = NSClassFromString(nameSpace + "." + childVcName) else {
         print("没有获取到字符串对应的Class")
         return
         }
         
         // 2.将对应的AnyObject转成控制器的类型
         guard let childVcType = childVcClass as? UIViewController.Type else{
         print("没有获取到对应控制器的类型")
         return
         }
         
         // 3.创建对应的控制器对象
         let childVc = childVcType.init()
         
         // 4.设置子控制器的属性
         childVc.title = title
         childVc.tabBarItem.image = UIImage(named: imageName)
         childVc.tabBarItem.selectedImage = UIImage(named: imageName + "_highlighted")
         
         // 5.包装导航控制器
         let childNav = UINavigationController(rootViewController: childVc)
         
         // 6.添加子控制器
         addChildViewController(childNav)
         */
   // }
}
// MARK:- 时间监听
extension MainViewController {
    // 时间监听本质发送消息,但是发送消息的OC的特性
    // 将方法包装成@SEL --> 类中查找方法列表 -->根据@SEL找到imp指针 --> 执行函数
    // 如果swift中将一个函数声明为private,那么该函数不会被添加到方法列表中
    // 如果在private前面加上@objc,那么该方法依然会被添加到方法列表
    @objc private func composeBtnClick() {
        //print("111")
    }
}









