//
//  UserAccountTool.swift
//  DB
//
//  Created by haixuan on 16/9/9.
//  Copyright © 2016年 华惠友. All rights reserved.
//

import UIKit

class UserAccountTool {
    //MARK:- 将类设计为单利
    static let shareInstace : UserAccountTool = UserAccountTool()
    
    // MARK:- 定义属性
    var account : UserAccount?
    
    //MARK:- 计算属性
    var accountPath : String {
        let accountPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!
        return (accountPath as NSString).stringByAppendingPathComponent("account.plist")
    }
    
    var isLogin : Bool {
        if account == nil {
            return false
        }
        
        guard let expiresDate = account?.expires_date else {
            return false
        }
        return expiresDate.compare(NSDate()) == NSComparisonResult.OrderedDescending
    }
    
    // MARK:- 重写init()函数
    init() {
        account = NSKeyedUnarchiver.unarchiveObjectWithFile(accountPath) as? UserAccount
    }  
}
